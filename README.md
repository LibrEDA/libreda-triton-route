<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# TritonRoute and TritonRoute-WXL bindings for LibrEDA

TritonRoute is an open-source VLSI detail router. TritonRoute-WXL is an improved version which includes a global routing engine.
This crate provides bindings to use both routers within the [LibrEDA framework](https://libreda.org).
The router is used as an external binary. Communication happens over LEF, DEF and 'guide' files.

The `TritonRoute` binary must be built manually from source:

```sh
git clone --recursive https://github.com/The-OpenROAD-Project/TritonRoute.git
cd TritonRoute
mkdir build
cd build
cmake ..
make
```

The path to the `TritonRoute` binary must then be specified in the API of the bindings.
In case of `TritonRoute-WXL` the two binary files `POST9.dat` and `POWV9.dat` must be copied to the current working directory. This files are necessary lookup-tables
for `flute`, an algorithm used to compute rectilinear Steiner minimal trees. (POST = potential optimal steiner tree, POWV = potential optimal wirelength vector, 9 = exact pre-computation for trees with up to 9 nodes).
